

Nombre: DevIn

Descripcion: Crear una "red social" orientado a desarrolladores de cualquier ambito en general.
Que tenga la posibilidad de ademas de crear un perfil:
-Un foro para consultas con otros devs sobre problemas en codigo (estilo stackOverflow)
-Un apartado para publicar proyectos o unirse a proyectos de otros devs con posibilidad de pago o gratuitos.
-Un apartado para busquedas de empleo sobre el rubro.
-La posibilidad de contactar con devs de lugares cercanos usando la localizacion del usuario.
-Publicar software libre - o cursos gratuitos para otros devs.
